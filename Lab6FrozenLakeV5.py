from pathlib import Path
from typing import NamedTuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

import gymnasium as gym
from gymnasium.envs.toy_text.frozen_lake import generate_random_map


sns.set_theme()

# %load_ext lab_black

class Params(NamedTuple):
    total_episodes: int  # Total episodes
    learning_rate: float  # Learning rate
    gamma: float  # Discounting rate
    epsilon: float  # Exploration probability
    map_size: int  # Number of tiles of one side of the squared environment
    seed: int  # Define a seed so that we get reproducible results
    is_slippery: bool  # If true the player will move in intended direction with probability of 1/3 else will move in either perpendicular direction with equal probability of 1/3 in both directions
    n_runs: int  # Number of runs
    action_size: int  # Number of possible actions
    state_size: int  # Number of possible states
    proba_frozen: float  # Probability that a tile is frozen
    savefig_folder: Path  # Root folder where plots are saved


params = Params(
    total_episodes=500,
    learning_rate=0.02,
    gamma=0.1,
    epsilon=0.1,
    map_size=8,
    seed=123,
    is_slippery=False,
    n_runs=20,
    action_size=None,
    state_size=None,
    proba_frozen=0.9,
    savefig_folder=Path("/img/"),
)
params

# Set the seed
rng = np.random.default_rng(params.seed)

# Create the figure folder if it doesn't exists
params.savefig_folder.mkdir(parents=True, exist_ok=True)

env = gym.make(
    "FrozenLake-v1",
    is_slippery=params.is_slippery,
    render_mode="rgb_array",
    desc=generate_random_map(
        size=params.map_size, p=params.proba_frozen, seed=params.seed
    ),
)

params = params._replace(action_size=env.action_space.n)
params = params._replace(state_size=env.observation_space.n)
print(f"Action size: {params.action_size}")
print(f"State size: {params.state_size}")


class Qlearning:
    def __init__(self, learning_rate, gamma, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.learning_rate = learning_rate
        self.gamma = gamma
        self.reset_qtable()

    def update(self, state, action, reward, new_state):
        """Update Q(s,a):= Q(s,a) + lr [R(s,a) + gamma * max Q(s',a') - Q(s,a)]"""
        delta = (
            reward
            + self.gamma * np.max(self.qtable[new_state, :])
            - self.qtable[state, action]
        )
        q_update = self.qtable[state, action] + self.learning_rate * delta
        return q_update

    def reset_qtable(self):
        """Reset the Q-table."""
        self.qtable = np.zeros((self.state_size, self.action_size))


class EpsilonGreedy:
    def __init__(self, epsilon):
        self.epsilon = epsilon

    def choose_action(self, action_space, state, qtable):
        """Choose an action `a` in the current world state (s)."""
        # First we randomize a number
        explor_exploit_tradeoff = rng.uniform(0, 1)

        # Exploration
        if explor_exploit_tradeoff < self.epsilon:
            action = action_space.sample()

        # Exploitation (taking the biggest Q-value for this state)
        else:
            # Break ties randomly
            # If all actions are the same for this state we choose a random one
            # (otherwise `np.argmax()` would always take the first one)
            if np.all(qtable[state, :]) == qtable[state, 0]:
                action = action_space.sample()
            else:
                action = np.argmax(qtable[state, :])
        return action
    
learner = Qlearning(
    learning_rate=params.learning_rate,
    gamma=params.gamma,
    state_size=params.state_size,
    action_size=params.action_size,
)
explorer = EpsilonGreedy(
    epsilon=params.epsilon,
)

def run_env():
    rewards = np.zeros((params.total_episodes, params.n_runs))
    steps = np.zeros((params.total_episodes, params.n_runs))
    episodes = np.arange(params.total_episodes)
    qtables = np.zeros((params.n_runs, params.state_size, params.action_size))
    all_states = []
    all_actions = []
    episode_rewards = []  # List to store episode rewards

    cumulative_reward = 0  # Variable to track cumulative reward across all runs

    for run in range(params.n_runs):  # Run several times to account for stochasticity
        learner.reset_qtable()  # Reset the Q-table between runs

        for episode in tqdm(
            episodes, desc=f"Run {run}/{params.n_runs} - Episodes", leave=False
        ):
            state = env.reset(seed=params.seed)[0]  # Reset the environment
            step = 0
            done = False
            total_rewards = 0

            while not done:
                action = explorer.choose_action(
                    action_space=env.action_space, state=state, qtable=learner.qtable
                )

                # Log all states and actions
                all_states.append(state)
                all_actions.append(action)

                # Take the action (a) and observe the outcome state(s') and reward (r)
                new_state, reward, terminated, truncated, info = env.step(action)

                done = terminated or truncated

                learner.qtable[state, action] = learner.update(
                    state, action, reward, new_state
                )

                total_rewards += reward
                cumulative_reward += reward  # Add reward to cumulative reward
                step += 1

                # Our new state is state
                state = new_state

            # Log all rewards and steps
            rewards[episode, run] = total_rewards
            steps[episode, run] = step
            episode_rewards.append(total_rewards)  # Append episode reward

        qtables[run, :, :] = learner.qtable
        """
        # Plot episode rewards
        plt.plot(episode_rewards)
        plt.xlabel("Episode")
        plt.ylabel("Reward")
        plt.title(f"Episode Rewards - Run {run+1}/{params.n_runs}")
        plt.show()
        #"""
    average_reward = cumulative_reward / (params.total_episodes * params.n_runs)  # Calculate average reward

    return rewards, steps, episodes, qtables, all_states, all_actions, average_reward


# Inside the main part of the code


def postprocess(episodes, params, rewards, steps, map_size):
    """Convert the results of the simulation in dataframes."""
    res = pd.DataFrame(
        data={
            "Episodes": np.tile(episodes, reps=params.n_runs),
            "Rewards": rewards.flatten(),
            "Steps": steps.flatten(),
        }
    )
    res["cum_rewards"] = rewards.cumsum(axis=0).flatten(order="F")
    res["map_size"] = np.repeat(f"{map_size}x{map_size}", res.shape[0])

    st = pd.DataFrame(data={"Episodes": episodes, "Steps": steps.mean(axis=1)})
    st["map_size"] = np.repeat(f"{map_size}x{map_size}", st.shape[0])
    return res, st


def qtable_directions_map(qtable, map_size):
    """Get the best learned action & map it to arrows."""
    qtable_val_max = qtable.max(axis=1).reshape(map_size, map_size)
    qtable_best_action = np.argmax(qtable, axis=1).reshape(map_size, map_size)
    directions = {0: "←", 1: "↓", 2: "→", 3: "↑"}
    qtable_directions = np.empty(qtable_best_action.flatten().shape, dtype=str)
    eps = np.finfo(float).eps  # Minimum float number on the machine
    for idx, val in enumerate(qtable_best_action.flatten()):
        if qtable_val_max.flatten()[idx] > eps:
            # Assign an arrow only if a minimal Q-value has been learned as best action
            # otherwise since 0 is a direction, it also gets mapped on the tiles where
            # it didn't actually learn anything
            qtable_directions[idx] = directions[val]
    qtable_directions = qtable_directions.reshape(map_size, map_size)
    return qtable_val_max, qtable_directions


res_all = pd.DataFrame()
st_all = pd.DataFrame()


env = gym.make(
        "FrozenLake-v1",
        is_slippery=params.is_slippery,
        render_mode="rgb_array",
        desc=generate_random_map(
            size=8, p=params.proba_frozen, seed=params.seed
        ),
)

env = gym.wrappers.RecordVideo(env,video_folder='vid', episode_trigger=lambda x: x==params.total_episodes - 1)
env = gym.wrappers.RecordVideo(env,video_folder='vid', episode_trigger=lambda x: x==params.n_runs*params.total_episodes - 1)
    

params = params._replace(action_size=env.action_space.n)
params = params._replace(state_size=env.observation_space.n)
env.action_space.seed(
    params.seed
)  # Set the seed to get reproducible results when sampling the action space
learner = Qlearning(
    learning_rate=params.learning_rate,
    gamma=params.gamma,
    state_size=params.state_size,
    action_size=params.action_size,
)
explorer = EpsilonGreedy(
    epsilon=params.epsilon,
)

print(f"Map size: 8x8")

rewards, steps, episodes, qtables, all_states, all_actions, average_reward = run_env()
# Print the average reward

print(f"Average Reward: {average_reward}")
# Save the results in dataframes

res, st = postprocess(episodes, params, rewards, steps, 8)
res_all = pd.concat([res_all, res])
st_all = pd.concat([st_all, st])
qtable = qtables.mean(axis=0)  # Average the Q-table between runs






"""--------------------Average Reward vs. Gamma-------------------------
gamma_values = [0.1, 0.2, 0.3, 0.4, 0.5,0.6, 0.7, 0.8, 0.9, 0.95]  # List of gamma values to iterate over
average_rewards = []

for gamma in gamma_values:
    params = params._replace(gamma=gamma)  # Update the gamma value in params
    
    # Rest of the code remains the same...

    print(f"Map size: 8x8")

    rewards, steps, episodes, qtables, all_states, all_actions, average_reward = run_env()
    # Print the average reward
    average_rewards.append(average_reward)  # Append average reward to the list

    print(f"Average Reward for gamma={gamma}: {average_reward}")
    # Save the results in dataframes

    res, st = postprocess(episodes, params, rewards, steps, 8)
    res_all = pd.concat([res_all, res])
    st_all = pd.concat([st_all, st])
    qtable = qtables.mean(axis=0)  # Average the Q-table between runs

print("gamma_values: ",gamma_values)
print("average_rewards: ",average_rewards)
plt.plot(gamma_values, average_rewards)
plt.xlabel("Gamma")
plt.ylabel("Average Reward")
plt.title("Average Reward vs. Gamma")
plt.savefig('AverageRewardvsGammaEpisodes'+str(params.total_episodes)+'.png')
#plt.show()
plt.clf()
#"""
"""---------------------Average Reward vs. Learning Rate-----------------
learning_rate_values = [0.1,0.2,0.3,0.4, 0.5,0.6, 0.7,0.8, 0.9]  # List of learning rate values to iterate over
average_rewards = []

for learning_rate in learning_rate_values:
    params = params._replace(learning_rate=learning_rate)  # Update the learning rate value in params
    
    # Rest of the code remains the same...

    print(f"Map size: 8x8")

    rewards, steps, episodes, qtables, all_states, all_actions, average_reward = run_env()
    # Print the average reward
    
    average_rewards.append(average_reward)  # Append average reward to the list

    print(f"Average Reward for learning_rate={learning_rate}: {average_reward}")
    # Save the results in dataframes

    res, st = postprocess(episodes, params, rewards, steps, 8)
    res_all = pd.concat([res_all, res])
    st_all = pd.concat([st_all, st])
    qtable = qtables.mean(axis=0)  # Average the Q-table between runs

print("learning_rate_values: ", learning_rate_values)
print("average_rewards: ", average_rewards)
plt.plot(learning_rate_values, average_rewards)
plt.xlabel("Learning Rate")
plt.ylabel("Average Reward")
plt.title("Average Reward vs. Learning Rate")
plt.savefig('AverageRewardvVsLearningRateEpisodes'+str(params.total_episodes)+'.png')
#plt.show()
plt.clf()
#"""

"""---------------------Average Reward vs. Epsilon-----------------
epsilon_values = [0.1, 0.2,0.3,0.4, 0.5,0.6, 0.7,0.8, 0.9]  # List of epsilon values to iterate over
average_rewards = []

for epsilon in epsilon_values:
    params = params._replace(epsilon=epsilon)  # Update the epsilon value in params
    
    # Rest of the code remains the same...

    print(f"Map size: 8x8")

    rewards, steps, episodes, qtables, all_states, all_actions, average_reward = run_env()
    # Print the average reward
    average_rewards.append(average_reward)  # Append average reward to the list

    print(f"Average Reward for epsilon={epsilon}: {average_reward}")
    # Save the results in dataframes

    res, st = postprocess(episodes, params, rewards, steps, 8)
    res_all = pd.concat([res_all, res])
    st_all = pd.concat([st_all, st])
    qtable = qtables.mean(axis=0)  # Average the Q-table between runs

print("epsilon_values: ", epsilon_values)
print("average_rewards: ", average_rewards)
plt.plot(epsilon_values, average_rewards)
plt.xlabel("Epsilon")
plt.ylabel("Average Reward")
plt.title("Average Reward vs. Epsilon")
plt.savefig('AverageRewardVsEpsilonEpisodes'+str(params.total_episodes)+'.png')
#plt.show()
plt.clf()
#"""
"""--------------------Optimal Value vs. Gamma for Different Epsilon Values----------------
epsilon_values = [0.1, 0.2, 0.3, 0.4, 0.5]  # List of epsilon values to iterate over
gamma_values = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95]  # List of gamma values to iterate over

# Create a figure with subplots
fig, ax = plt.subplots()

for epsilon in epsilon_values:
    optimal_values = []

    for gamma in gamma_values:
        params = params._replace(gamma=gamma, epsilon=epsilon)  # Update the gamma and epsilon values in params

        # Rest of the code remains the same...

        print(f"Map size: 8x8")

        rewards, steps, episodes, qtables, all_states, all_actions, average_reward = run_env()

        optimal_value = np.max(qtables.mean(axis=0))  # Find the optimal value by taking the maximum value from the averaged Q-table
        optimal_values.append(optimal_value)  # Append the optimal value to the list

        print(f"Optimal Value for epsilon={epsilon}, gamma={gamma}: {optimal_value}")
        # Save the results in dataframes

        res, st = postprocess(episodes, params, rewards, steps, 8)
        res_all = pd.concat([res_all, res])
        st_all = pd.concat([st_all, st])
        qtable = qtables.mean(axis=0)  # Average the Q-table between runs

    ax.plot(gamma_values, optimal_values, label=f"Epsilon = {epsilon}")  # Plot the optimal values for each gamma value

ax.set_xlabel("Gamma")
ax.set_ylabel("Optimal Value")
ax.set_title("Optimal Value vs. Gamma for Different Epsilon Values")
ax.legend()
plt.savefig('OptimalValueVSGammaForDifferentEpsilonValuesEpisodes'+str(params.total_episodes)+'.png')
#plt.show()
#"""




env.close()



